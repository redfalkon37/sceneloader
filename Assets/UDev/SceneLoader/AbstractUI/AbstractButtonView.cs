using UnityEngine;
using UnityEngine.UI;

///<summary>
/// Abstract button
///</summary>
[RequireComponent(typeof(Button))]
public abstract class AbstractButtonView : MonoBehaviour
{
    protected Button button = default;

    protected virtual void Awake() 
    {
        button = this.GetComponent<Button>();
        button.onClick.AddListener(OnButtonClicked);
    }

    protected virtual void OnDestroy() 
    {
        if (button != null)
        {
            button.onClick.RemoveListener(OnButtonClicked);
        }
    }
    ///<summary>
    /// OnButtonClick method
    ///</summary>
    public abstract void OnButtonClicked();
}
