using UnityEngine;
using UnityEngine.SceneManagement;

namespace UDev.SceneController
{
    ///<summary>
    /// Next scene Load Button
    ///</summary>
    public class LoadButton : AbstractButtonView
    {
        [SerializeField]
        private string sceneNextName = default; // next scene name

        private const string layerScaneName = "Loading";
        public override void OnButtonClicked()
        {
            LoadSceneController.NextSceneLoad = sceneNextName;
            SceneManager.LoadScene(layerScaneName);
        }
    }
}