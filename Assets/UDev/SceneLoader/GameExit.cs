using UnityEngine;

/// <summary>
/// Game Exit
/// </summary>
public class GameExit : AbstractButtonView
{
    /// <summary>
    /// Exit
    /// </summary>
    public override void OnButtonClicked() => Application.Quit();
}
