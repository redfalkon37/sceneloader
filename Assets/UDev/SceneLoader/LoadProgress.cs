using UnityEngine;
using UnityEngine.UI;

namespace UDev.SceneController
{
    /// <summary>
    /// Load Scene Process
    /// </summary>
    public class LoadProgress : MonoBehaviour
    {
        private Image fillImage = default;

        private void Awake()
        {
            fillImage = GetComponent<Image>();
            LoadSceneController.onSceneChangeProgress += UpdateProcess;
        }

        private void UpdateProcess(float value) => fillImage.fillAmount = value;

        private void OnDestroy() => LoadSceneController.onSceneChangeProgress -= UpdateProcess;
    }
}