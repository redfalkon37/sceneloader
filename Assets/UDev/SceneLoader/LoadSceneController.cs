using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UDev.SceneController
{
    ///<summary>
    /// Scene Loading Logic
    ///</summary>
    public class LoadSceneController : MonoBehaviour
    {
        [SerializeField]
        private float delayPreLoad = 3.0f;

        /// <summary>
        /// Scene load Process action
        /// </summary>
        public static event Action<float> onSceneChangeProgress = delegate { };
        /// <summary>
        /// Start Load
        /// </summary>
        public static event Action onLoadingStart = delegate { };

        private AsyncOperation loadingOperation = default; // progress
        private float loadingValue = default;
        private Coroutine loadingStart = default;
        private static bool loadedOnce = false;

        public static string NextSceneLoad = "Menu";

        public float LoadingValue
        {
            get => loadingValue;
            private set
            {
                if (loadingValue != value)
                {
                    loadingValue = value;
                    onSceneChangeProgress(loadingValue);
                }
            }
        }

        private void Awake()
        {
            if (loadingStart == null)
            {
                loadingStart = StartCoroutine(LoadingAsync());
            }
            else
            {
                StopCoroutine(loadingStart);
                loadingStart = StartCoroutine(LoadingAsync());
            }
        }

        private IEnumerator LoadingAsync()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            float resultPreLoad = loadedOnce ? 0f : delayPreLoad;
            loadedOnce = true;
            yield return new WaitForSecondsRealtime(resultPreLoad);
            yield return loadingOperation;
            loadingOperation = SceneManager.LoadSceneAsync(NextSceneLoad);
            loadingOperation.allowSceneActivation = false;

            onLoadingStart();

            while (!loadingOperation.isDone)
            {
                loadingValue = loadingOperation.progress;
                Debug.Log(loadingValue);

                if (loadingValue >= 0.9f)
                {
                    loadingOperation.allowSceneActivation = true;
                }
                yield return null;
            }
        }

        private void OnDestroy()
        {
            if (loadingStart != null)
            {
                StopCoroutine(loadingStart);
                loadingStart = null;
            }
        }
    }
}